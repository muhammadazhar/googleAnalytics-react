
import './App.css';
import React, { useEffect } from 'react';
import ReactGA from "react-ga4";
import {
  BrowserRouter as Router,
  Routes, Route,
  Link
} from "react-router-dom";
import FirstPage from './firstpage';
import SecondPage from './secondPage';
import Home from './home';
function App() {
  useEffect(() => {
    ReactGA.initialize("G-4L8ZPJFT2D");
    // reporting page view 
    console.log(window.location.pathname + window.location.search)
    ReactGA.send({ hitType: "pageview", page: window.location.pathname + window.location.search })
  }, [])

  return (
    <div className="App">
      <Router>
        <nav>
          <ul>
            <li>
              <Link to="/firstpage">First Page</Link>
            </li>
            <li>
              <Link to="/secondpage">Second Page</Link>
            </li>
            <li>
              <Link to="/home">home </Link>
            </li>

          </ul>
        </nav>
        <Routes>
          <Route path="/firstpage" element={<FirstPage />}>

          </Route>
          <Route path="/secondpage" element={<SecondPage />}>

          </Route>
          <Route path="/home" element={<Home />}>

          </Route>
        </Routes>



      </Router>

    </div>
  );
}

export default App;
