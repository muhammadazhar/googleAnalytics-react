import React from "react";
import ReactGA from "react-ga4";
const Home = () => {

    const eventTrack = () => {
        ReactGA.event({
            category: "button",
            action: "player_btn"
        });
        alert('done')
    }
    return (
        <>
            <h1>HOME</h1>
            <button onClick={() => { eventTrack() }}>CLICK ME!</button>

        </>


    )
}
export default Home;